package com.example.quizapp;

public class Questions {

    // Required Variables Declaration

    private String questionsId;
    private boolean answers;

    // Getters and Setters

    public String getQuestionsId() {
        return questionsId;
    }

    public void setQuestionsId(String questionsId) {
        this.questionsId = questionsId;
    }

    public boolean getAnswers() {
        return answers;
    }

    public void setAnswers(boolean answers) {
        this.answers = answers;
    }

    // Constructors

    public Questions(String questionsId, boolean answers) {
        this.questionsId = questionsId;
        this.answers = answers;
    }

    public  Questions() {}
}
